namespace App\Http\Controllers;

use App\Http\Library\ApiHelpers;
use App\GuestBook;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;

class GuestBookController extends Controller
{
    use ApiHelpers;

    public function guestbook(Request $request): JsonResponse
    {

        if ($this->isAdmin($request->user()) || || $this->isGuest($request->user())) {
            $guestbook = DB::table('guestbooks')->get();
            return $this->onSuccess($guestbook, 'GuestBook Retrieved');
        }

        return $this->onError(401, 'Unauthorized Access');
    }

    public function singleGuestbook(Request $request, $id): JsonResponse
    {
        $user = $request->user();
        if ($this->isAdmin($user) || $this->isGuest($user)) {
            $guestbook = DB::table('guestbooks')->where('id', $id)->first();
            if (!empty($guestbook)) {
                return $this->onSuccess($guestbook, 'GuestBook Retrieved');
            }
            return $this->onError(404, 'GuestBook Not Found');
        }
        return $this->onError(401, 'Unauthorized Access');
    }

    public function createGuestbook(Request $request): JsonResponse
    {

        $user = $request->user();
        if ($this->isAdmin($user) || $this->isGuest($user)) {
            $validator = Validator::make($request->all(), $this->guestbookValidationRules());
            if ($validator->passes()) {
                // Create New GuestBook
                $guestbook = new GuestBook();
                $guestbook->name = $request->input('name');
                $guestbook->address = Str::slug($request->input('address'));
                $guestbook->phone = $request->input('phone');
                $guestbook->note = $request->input('note');
                $guestbook->save();

                return $this->onSuccess($guestbook, 'GuestBook Created');
            }
            return $this->onError(400, $validator->errors());
        }

        return $this->onError(401, 'Unauthorized Access');

    }

    public function updateGuestbook(Request $request, $id): JsonResponse
    {
        $user = $request->user();
        if ($this->isAdmin($user) || $this->isGuest($user)) {
            $validator = Validator::make($request->all(), $this->guestbookValidationRules());
            if ($validator->passes()) {
                // Create New GuestBook
                $guestbook = GuestBook::find($id);
                $guestbook->name = $request->input('name');
                $guestbook->address = Str::slug($request->input('address'));
                $guestbook->phone = $request->input('phone');
                $guestbook->note = $request->input('note');
                $guestbook->save();

                return $this->onSuccess($guestbook, 'GuestBook Updated');
            }
            return $this->onError(400, $validator->errors());
        }

        return $this->onError(401, 'Unauthorized Access');
    }

    public function deleteGuestbook(Request $request, $id): JsonResponse
    {
        $user = $request->user();
        if ($this->isAdmin($user)) {
            $guestbook = GuestBook::find($id); // Find the id of the guestbook passed
            $guestbook->delete(); // Delete the specific guestbook data
            if (!empty($guestbook)) {
                return $this->onSuccess($guestbook, 'GuestBook Deleted');
            }
            return $this->onError(404, 'GuestBook Not Found');
        }
        return $this->onError(401, 'Unauthorized Access');
    }

}
