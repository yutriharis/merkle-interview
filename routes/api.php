<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:sanctum'], function() {
    // list all posts
    Route::get('posts', [GuestBookController::class, 'guestbook']);
    // get a post
    Route::get('posts/{id}', [GuestBookController::class, 'singleGuestbook']);
    // add a new post
    Route::post('posts', [GuestBookController::class, 'createGuestbook']);
    // updating a post
    Route::put('posts/{id}', [GuestBookController::class, 'updateGuestbook']);
    // delete a post
    Route::delete('posts/{id}', [GuestBookController::class, 'deleteGuestbook']);
});
